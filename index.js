var createGame = require('voxel-engine');
var highlight = require('voxel-highlight')
var player = require('voxel-player')
var world = require('./world.js');
var fly = require('voxel-fly')
var walk = require('voxel-walk')

var opts = {
    texturePath: './assets/textures/',
    // generate: world.generator,
    chunkSize: 16,
    chunkDistance: 2,
    fogScale: 16,
    lightsDisabled: true,
    generateChunks: false,
    materials: [
        ['grass', 'dirt', 'grass_dirt'], 'dirt', 'sand', 'cobblestone', 'ice', 'clouds', ['log_oak_top', 'log_oak_top', 'log_oak'], 'leaves_oak'
    ]
}

var game = createGame(opts);
var container = document.getElementById('container');
game.appendTo(container);

var createPlayer = player(game);
var dude = createPlayer('./assets/textures/shama.png');
dude.possess();
dude.yaw.position.set(0, 5, 0);

var generateChunk = world();
game.voxels.on('missingChunk', function (p) {
    var voxels = generateChunk(p, game.chunkSize)
    var chunk = {
        position: p,
        dims: [game.chunkSize, game.chunkSize, game.chunkSize],
        voxels: voxels
    }
    game.showChunk(chunk)
})

var makeFly = fly(game)
var target = game.controls.target()
game.flyer = makeFly(target)

var position = [0, 0, 0];

game.on('tick', function() {
    walk.render(target.playerSkin)
    var vx = Math.abs(target.velocity.x)
    var vz = Math.abs(target.velocity.z)
    if (vx > 0.001 || vz > 0.001) walk.stopWalking()
    else walk.startWalking()

    position = game.raycast().voxel;
})

//cloud
// var clouds = require('voxel-clouds')({
//     game: game,
//     high: 12,
//     distance: 300,
//     many: 100,
//     speed: 0.01,
//     // material of the clouds
//     material: new game.THREE.MeshBasicMaterial({
//         emissive: 0xffffff,
//         shading: game.THREE.FlatShading,
//         fog: false,
//         transparent: true,
//         opacity: 0.5,
//     }),
// });
// game.on('tick', clouds.tick.bind(clouds));

window.addEventListener('keydown', function (ev) {
    if (ev.keyCode === 'T'.charCodeAt(0)) dude.toggle()
});

var highlighter = highlight(game, {adjacentActive:function () { return game.controls.state.firealt}});

var CreateCube = function(src){
    const geometry = new game.THREE.CubeGeometry(1.01, 1.01, 1.01);
    const material = new game.THREE.MeshLambertMaterial({
        transparent: true,
        opacity: 1
    });
    const cube = new game.THREE.Mesh(geometry, material);
    const image = new Image();

    image.onload = function (img) {

        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');

        canvas.width = 512;
        canvas.height = 512;

        ctx.drawImage(this, 0, 0, canvas.width, canvas.height);

        material.map = new game.THREE.Texture(canvas);
        material.map.needsUpdate = true;

    };

    image.crossOrigin = "anonymous";
    image.src = src;

    return cube;
};

// second version
// var cubic;
// var CreateCubeByTexture = function () {
//     cubic = texture({
//         game: game,
//         texturePath: './assets/destroy/destroy_stage_9.png',
//         materialType: game.THREE.MeshNormalMaterial({
//             transparent: true,
//             opacity: 1
//         }),
//         materialParams: {},
//         materialFlatColor: true
//     });
// };

var cube = [];
async function createCubeGroup(){
    for (let i = 0 ; i < 10; i++) {
        cube[i] = await CreateCube('./assets/destroy/destroy_stage_'+i+'.png');
    }
}
createCubeGroup();

window.addEventListener('keydown', function (ev) {
    if (ev.which === 'R'.charCodeAt(0)) {
        console.log("highlighter pos : ",highlighter.mesh.position);
        console.log("highlighter scale : ",highlighter.mesh.scale);
        var val = game.raycast().value;
        console.log("block : ", val);
        // for( let i = 0; i <= 10; i++ ) {
        //     window.setTimeout(function() {
        //         if(i === 10){
        //             game.scene.remove(cube[i-1]);
        //             return;
        //         }
        //         cube[i].position = highlighter.mesh.position;
        //         // cube[i].scale.set(10,10,10);
        //         game.scene.add(cube[i]);
        //         game.scene.remove(cube[i-1] ? cube[i-1] : null);
        //     }, 500 * i);
        // }
    }
});

container.addEventListener("mouseup", function (event) {
    var currentMaterial = 1
    if (event.button === 0) {
        clearInterval(eventNumber);
        game.scene.remove(cube[b-1]);
        b = 0;

    } else if (event.button === 2) {
        var position = highlighter.currVoxelAdj;
        if (position) {
            game.createBlock(position, currentMaterial);
        }
    }
});

var eventNumber;
var b = 0;

container.addEventListener("mousedown" , function (event) {
    if(event.button === 0) {
        eventNumber = setInterval(function () {
            //console.log("dig " + b);
            if(b === 10){
                game.scene.remove(cube[b-1]);
                clearInterval(eventNumber);
                var position = highlighter.currVoxelPos;
                if(position) {
                    game.setBlock(position, 0);
                }
                return;
            }
            cube[b].position = highlighter.mesh.position;
            // cube[i].scale.set(10,10,10);
            game.scene.add(cube[b]);
            game.scene.remove(cube[b-1] ? cube[b-1] : null);
            b++;
        }, 500)
    }
});

// var createSky = require('voxel-sky')(game);
// var sky = createSky(function(time) {
  
//     // spin the sky once per day
//     this.spin(Math.PI * 2 * (time / 2400));
  
//     // Change the sky pink at 7AM
//     if (time === 700) this.color(new this.game.THREE.Color(0xFF36AB), 1000);
  
//     if (time === 0) {
//       // paint a green square on the bottom (above your head at 1200)
//       this.paint('bottom', function() {
//         // The HTML canvas and context for each side are accessible
//         this.context.rect((this.canvas.width/2)-25, (this.canvas.height/2)-25, 50, 50);
//         this.context.fillStyle = this.rgba(0, 1, 0, 1);
//         this.context.fill();
//       });
  
//       // paint 500 stars on the top and bottom
//       this.paint(['top', 'bottom'], this.stars, 500);
  
//       // paint a big red full moon on every face
//       // 0 = full, 0.25 quarter waxing, 0.5 = new, 0.75 quarter waning
//       this.paint('all', this.moon, 0, 100, new this.game.THREE.Color(0xFF0000));
  
//       // paint a small green sun on each side face
//       this.paint('sides', this.sun, 10, new this.game.THREE.Color(0x00FF00));
//     }
  
//     if (time === 400) {
//       // clear the canvas on all sides at 4AM
//       this.paint('all', this.clear);
//     }
  
//     if (time === 1800) {
//       // lower the sunlight intensity at 6PM
//       this.sunlight.intensity = 0.2;
  
//       // set the ambient color (is a hemisphere light)
//       this.ambient.color.setHSL(0.9, 1, 1);
//     }
//   });
//   game.on('tick', sky);

var createSky = require('voxel-sky')(game);
var sky = createSky();
game.on('tick', sky);